// Initialize Firebase
	  var config = {
	    apiKey: "AIzaSyC9WQtInnuEGRv6g3t4joQFbYsKh3eXMDc",
	    authDomain: "sample-f463f.firebaseapp.com",
	    databaseURL: "https://sample-f463f.firebaseio.com",
	    projectId: "sample-f463f",
	    storageBucket: "sample-f463f.appspot.com",
	    messagingSenderId: "554381220633"
	  };
	  firebase.initializeApp(config);

			function writeUserData(username, email, password) {
			  firebase.database().ref('users/').push({
			    username: username,
			    email: email,
			    password: password
			  }, function(error) {
				  if (error) {
				    alert("Data could not be saved." + error);
				  } else {
				    alert("Data saved successfully.");
				    $('#add_form').trigger("reset");
				  }
				});
			}

			function updateUserData(username, email, password) {
			  firebase.database().ref('users/-LNTv_1ewEENCWDqfBsh').update({
			    username: username,
			    email: email,
			    password: password
			  }, function(error) {
				  if (error) {
				    alert("Data could not be saved." + error);
				  } else {
				    alert("Data update successfully.");
				    $('#add_form').trigger("reset");
				  }
				});
			}

			function edit_user_data(){
				var username = $('#username').val();
				var email = $('#email').val();
				var password = $('#password').val();
				updateUserData(username,email,password);
			}
			
			function add_user_data(){
				var username = $('#username').val();
				var email = $('#email').val();
				var password = $('#password').val();
				writeUserData(username,email,password);
			}

			var users = firebase.database().ref('users');
			users.on("value", function(snapshot){
    var users_data = JSON.stringify(snapshot.val(), null, 2);
    $('.row_data').remove();
    $.each( snapshot.val(), function( key, value ) {
				  $('#users_table_data').append('<tr class="row_data"><td>'+value.username+'</td><td>'+value.email+'</td><td>'+value.password+'</td><td><a href="edit_user.html?id='+key+'">Edit</a> | <a href="del_user.html?id='+key+'">Delete</a></td></tr>');
				});
			});